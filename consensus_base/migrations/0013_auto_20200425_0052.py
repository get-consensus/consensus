# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2020-04-25 00:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('consensus_base', '0012_pollinvitation_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pollinvitation',
            name='token',
            field=models.CharField(max_length=64, unique=True),
        ),
    ]
