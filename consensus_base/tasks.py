from __future__ import absolute_import, unicode_literals
from celery import shared_task
from time import sleep

from celery.decorators import periodic_task
from celery.task.schedules import crontab
from celery.utils.log import get_task_logger
from consensus_base.mail import send_invitations

logger = get_task_logger(__name__)


@shared_task
def simulate_send_emails(num_emails):
    for i in range(1, num_emails):
        print('Sending email %d' % i)
        sleep(1)
    return 'Emails sent'


@periodic_task(run_every=(crontab(minute='*/1')), ignore_result=False)
def cron_send_invitations():
    logger.info("Send invitations task")
    send_invitations()
    return 'Invitations sent'
