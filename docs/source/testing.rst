How To Testing
==============

Running the tests suites
^^^^^^^^^^^^^^^^^^^^^^^^

#. Starting a Docker compose instances for testing::

       ./tests/setup-test-server.sh
       ./tests/run-test-server.sh

#. Running the unitary tests suites::

       ./tests/run-tox.sh

#. Running the integration tests suites::

       ./tests/run-robot.sh

#. Stopping the Docker compose instances::

       ./tests/stop-test-server.sh

All at once::

   ./tests/setup-test-server.sh
   ./tests/run-test-server.sh
   ./tests/run-tox.sh
   ./tests/run-robot.sh
   ./tests/stop-test-server.sh


