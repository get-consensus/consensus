FROM tiangolo/uwsgi-nginx:python3.7

# Copy the website to the /app folder
COPY . /app/

RUN apt-get update
RUN apt-get install -y build-essential python3-dev libldap2-dev libsasl2-dev
RUN apt-get install -y redis-server

# Install all our dependencies
WORKDIR /app
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

RUN apt-get remove -y build-essential python3-dev libldap2-dev libsasl2-dev
RUN apt-get clean
