#! /bin/bash -x
docker run hello-world
docker-compose --version

./docker-compose.sh up --no-start --force-recreate
./docker-compose.sh up -d
./docker-compose.sh ps
./docker-compose.sh exec -T app "./manage.py wait_for_db"
./docker-compose.sh exec -T app "./manage.py migrate"
./docker-compose.sh exec -T app "./manage.py createsuperuser2 --noinput --username root --password root --email r@ignore.com"

