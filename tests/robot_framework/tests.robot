*** Settings ***
Documentation     Simple test using SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${BROWSER}        Firefox
${POLLS URL}      http://%{DJANGO_DOMAIN=localhost}/polls
${ADMIN URL}      http://%{DJANGO_DOMAIN=localhost}/admin/

*** Test Cases ***
Open
    Open

Test Login
    Unauthorized
    Login

Test Polls
    Open Polls

Close
    Close All Browsers


*** Keywords ***
Open
    ${firefox options} =     Evaluate    sys.modules['selenium.webdriver'].firefox.webdriver.Options()    sys, selenium.webdriver
    Call Method       ${firefox options}   add_argument    -headless
    Create Webdriver    ${BROWSER}    firefox_options=${firefox options}
    Set Window Size    1500    1500
    Go To    ${POLLS URL}
    Element Text Should Be   //h2[1]    Sign-in with:

Unauthorized
    Go To      ${ADMIN URL}
    Title Should Be    Log in | Django site admin

Login
    Go To      ${ADMIN URL}
    Input Text    username    root
    Input Text    password    root
    Click Button    Log in
    Location Should Be    ${ADMIN URL}
    Title Should Be    Site administration | Django site admin
    Page Should Contain    Django administration

Open Polls
    Go To      ${POLLS URL}
    Element Text Should Be   //h1[1]    Polls


