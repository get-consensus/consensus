#! /bin/sh

mkdir -p docker-compose/secrets/

cp django_consensus/local_settings.py.sample docker-compose/secrets/local_settings.py
cp docker-compose/app/conf/redis.conf docker-compose/secrets/
cp docker-compose/app/conf/supervisor-redis.conf.sample docker-compose/secrets/supervisor-redis.conf
cp docker-compose/app/conf/supervisor-celery.conf.sample docker-compose/secrets/supervisor-celery.conf
cp setup-env.sh.sample.postgres setup-env.sh

