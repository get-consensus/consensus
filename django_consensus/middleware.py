from django.contrib.auth.middleware import RemoteUserMiddleware
from django.conf import settings


class CustomHeaderMiddleware(RemoteUserMiddleware):
    if hasattr(settings, 'CONSENSUS_REMOTE_USER_HEADER'):
        header = settings.CONSENSUS_REMOTE_USER_HEADER
